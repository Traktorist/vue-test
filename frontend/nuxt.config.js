module.exports = {
    head: {
        title: 'vue-test-cities',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: 'vue test project'}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
            {rel: 'stylesheet', type: 'text/css', href: 'https://use.fontawesome.com/releases/v5.2.0/css/all.css'}
        ]
    },
    loading: {color: '#3B8070'},
    build: {
        extend(config, {isDev, isClient}) {
            if (isDev && isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })
            }
        },
        postcss: {
            plugins: {
                'postcss-custom-properties': false
            }
        }
    },
    plugins: [
        {src: '~/plugins/buefy'}
    ]
};
