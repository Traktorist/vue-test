import Vue from 'vue'
import Buefy from 'buefy'
import 'buefy/dist/buefy.min.css'

Vue.use(Buefy, { defaultIconPack: 'fas' });