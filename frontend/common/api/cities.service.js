import cities from '~/common/dummy/data.json'
const CitiesService = {
    async getCities() {
        return cities;
    }
}

export default CitiesService;