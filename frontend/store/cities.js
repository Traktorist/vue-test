import CitiesService from '~/common/api/cities.service';

export const state = () => ({
    cities: []
});

export const mutations = {
    addCity(state, city) {
        state.cities.push(city)
    },
    setCities(state, cities) {
        state.cities = cities
    },
    deleteCity(state, id) {
        state.cities.splice(id, 1)
    }
};

export const actions = {
    async getCities({commit}) {
        let cities = await CitiesService.getCities();
        if (cities)
            commit('setCities', cities)
    },
    async addCity({commit}, payload) {
        let city = {
            name: payload.name,
            temp: payload.temp ? payload.temp : '-',
            wind: payload.wind && payload.direction ? payload.wind + ` m/s ${payload.direction}` : '-',
            pressure: payload.pressure ? payload.pressure + ' mm Hg. Art.': '-'
        };

        commit('addCity', city);
    },
    async deleteCity({commit}, id) {
        commit('deleteCity', id)
    }
};